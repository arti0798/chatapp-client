async function register(url) {
    try {
  
        const firstName = document.getElementById("firstName").value;
        const lastName = document.getElementById("lastName").value;
        const email = document.getElementById("email").value;
        const phoneNumber = document.getElementById("phoneNumber").value;
        const password = document.getElementById("password").value;
        const confirm = document.getElementById("confirmPassword").value;
        
        if(password == '') {

            document.getElementById('msg').innerHTML="**Please Enter Password";
        }
        else if(password != confirm) {

            document.getElementById('msg').innerHTML="** Password and confirm password should be same";
            document.getElementById('password').value=null;
            document.getElementById('confirmPassword').value=null;
    }
    else {
        const user = {

            firstName:firstName,
            lastName:lastName,
            email:email,
            mobNo:phoneNumber,
            password:password
        };
        //}
        const promiseResponse = await fetch(url+"/user/add", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;charset=utf-8'
            },
            body: JSON.stringify(user),
        });
      
      console.log(promiseResponse);
      if(promiseResponse.status === 200) {

        window.location.replace("login.php");
      }
      if(promiseResponse.status === 400) {

        document.getElementById("body").innerHTML="User Already Exist !";
      }
      /*  const addUserResponse = await promiseResponse.json();
        alert('b7dsghd'); 
        console.log(addUserResponse);
        if (addUserResponse.status === 200) {
            alert('Im in 200');
            window.location.replace("../view/login.php");
            alert('Im in 200');

        }
        if (addUserResponse.status === 400) {
            alert('i m 400');
            document.getElementById("body").innerHTML="User Already Exist !"; 
        }
        */
      
    }
    }
    catch (error) {
        
        console.log(error);
    }
}
