async function getDetails(url) {
    
    try {
        
        
        const userList = await fetch(url+"/user/list");
        console.log("UserList ", userList);

        const response = await userList.json();

        console.log(response);
    }
    catch (error) {

        console.log(error);
    }
}