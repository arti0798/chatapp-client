<?php
	$ini_array = parse_ini_file("../config/config.ini",true);
	$url = $ini_array['url'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/web.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <title>0 Miles</title>
</head>
<body>
    <div class="logo">
                <img src="./photos/0miles.png"  alt="">
    </div>
    <div class="container">
        <div class="header">
            <h3>Recent Chats</h3>
        </div>
        <br>
        <br>
        <br>
        <div class="msg-header">
            
            <div class="msg-header-img">
                <img src="./photos/1499345224_female1.png"  alt="">
            </div>

            <div class="active">
                <h4>Shruti(User Name)</h4>
                <h6>3 hrs ago</h6>
                <div class="link">    
                    <a href="chat.html">Chat</a>
                </div>
                        
            </div>
  
        </div>
   
        <div class="msg-header">
        
            <div class="msg-header-img">
                <img src="./photos/1499345224_female1.png" alt="">
            </div>
            
            <div class="active">
                <h4>(User Name)</h4>
                <h6>Last seen
                </h6>
                <div class="link">    
                    <a href="chat.html">Chat</a>
                </div>
                
            </div>
        </div>
        <div class="msg-header">
        
            <div class="msg-header-img">
                <img src="./photos/1499345471_boy.png" alt="">
            </div>
            
            <div class="active">
                <h4>(User Name)</h4>
                <h6>Last seen
                </h6>
                <div class="link">    
                    <a href="chat.html">Chat</a>
                </div>
                
            </div>
        </div>
        <div class="msg-header">
        
            <div class="msg-header-img">
                <img src="./photos/1499345224_female1.png" alt="">
            </div>
            
            <div class="active">
                <h4>(User Name)</h4>
                <h6>Last seen
                </h6>
                <div class="link">    
                    <a href="chat.html">Chat</a>
                </div>
                
            </div>
        </div>
        <div class="msg-header">
        
            <div class="msg-header-img">
                <img src="./photos/1499345471_boy.png" alt="">
            </div>
            
            <div class="active">
                <h4>(User Name)</h4>
                <h6>Last seen
                </h6>
                <div class="link">    
                    <a href="chat.html">Chat</a>
                </div>
                
            </div>
        </div>
        <div class="msg-header">
        
            <div class="msg-header-img">
                <img src="./photos/1499345224_female1.png" alt="">
            </div>
            
            <div class="active">
                <h4>(User Name)</h4>
                <h6>Last seen
                </h6>
                <div class="link">    
                    <a href="chat.html">Chat</a>
                </div>
                
            </div>
        </div>
        
        <!--<div class="chat-page">
            <div class="msg-inbox">
                <div class="chats">
                    <div class="msg-page">
                        <div class="received-chats">
                            <div class="received-chats-img">
                                <img src="./photos/1499345224_female1.png" alt="">
                            </div>
                            <div class="received-msg">
                                <div class="received-msg-inbox">
                                    <p>Hiiii!! This is message form Shruti</p>
                                    <span class="time"> 11.01 pm</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    -->
		
</body>
<script src = "./js/getDetail.js"></script>
</html>
