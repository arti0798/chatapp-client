<?php
    $ini_array = parse_ini_file("../config/config.ini",true);
    $url = $ini_array['url'];
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="./css/styleregister.css">

    </head>
<body background="../public /photos/IMG_20170224_120557_1487936107848.jpg"> 

        <div class="main">
                <ul>
                    <li class="active"> <a href="index.php">Home</a></li>
                    <li> <a href="login.php">Login</a></li>
                    <li> <a href="register.php">Register</a></li>
                </ul>
            </div>
    <div class="register">
        
        <div class="login-box" >
        <h1>  Register   </h1>
            <div class="textbox">
                <i class="fas fa-user" aria-hidden="true"></i>
                <input type="text"   class="text" id="firstName" placeholder="FIRST NAME">
                
            </div>
            <div class="textbox">
                <i class="fas fa-user" aria-hidden="true"></i>
                <input type="text"   class="text" id="lastName" placeholder="LAST NAME">
                
            </div>
                
            <div class="textbox">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <input type="email"   class="text" id="email" placeholder="EMAIL">
            </div>
                        
            <div class="textbox">
                <i class="fa fa-mobile" aria-hidden="true"></i>
                <input type="text" class="text" id="phoneNumber" placeholder="PHONE NUMBER" >
            </div>
            

            <div class="textbox">
                <i class="fas fa-lock"></i>
                <input type="password" id="password" placeholder="PASSWORD">
            </div>

            <span id="msg" style="color: red; font-size: 10px;"></span>

             <span id="body" style="color: red; font-size: 10px;"></span>

            <div class="textbox">
                <i class="fas fa-lock"></i>
                <input type="password" id="confirmPassword" placeholder=" CONFIRM PASSWORD">
            </div>

            <button type="submit" class="btn btn-success btn-block"  onclick=register(<?php echo(json_encode($url));?>); >
                <a href="../public /login.php"></a>
            Submit</button>
        </div>
   
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
    </div>
</body>
<script src="./js/register.js"></script>
</html>
