<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/forgetpwd.css">
    <title>Document</title>
</head>
<body>
    <div class="login">
     
        <div class="login-box" >
        <h1>Forgot password</h1>
        
        <div class="textbox">
          <i class="fas fa-user"></i>
          <input type="text" placeholder="Email id" id="email">
        </div>
  
        <span id="msg" style="color: red; font-size: 15px;">
        </span>
  
        <input type="button" class="btn" value="Sign in" onclick=login(<?php echo(json_encode($url)); ?>) class="btn btn-success btn-block">
        <a href="#path">Back to login</a>
      </div>
      </div>
  
</body>
</html>