<?php
  $ini_array = parse_ini_file("../config/config.ini", true);
  $url = $ini_array['url'];
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="./css/stylelogin.css">
  </head>
  <body background="../public/photos/DSC_0346.JPG">
    

    <div class="main">
        <ul>
            <li class="active"> <a href="index.php">Home</a></li>
            <li> <a href="login.php">Login</a></li>
            <li> <a href="register.php">Register</a></li>
        </ul>
    </div>
    <div class="login">
     
      <div class="login-box" >
      <h1>Login</h1>
      
      <div class="textbox">
        <i class="fas fa-user"></i>
        <input type="text" placeholder="Email id" id="email">
      </div>

      <div class="textbox">
        <i class="fas fa-lock"></i>
        <input type="password" placeholder="Password" id="password">
      </div>
      <span id="msg" style="color: red; font-size: 15px;">
      </span>

      <input type="button" class="btn" value="Sign in" onclick=login(<?php echo(json_encode($url)); ?>) class="btn btn-success btn-block">
      <a href="#path">Forget Password?</a>
    </div>
    </div>
  </body>
  <script src="./js/login.js"></script>
</html>
